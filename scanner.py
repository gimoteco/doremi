#-*- coding: utf-8 -*-
import os
import sqlite3
import mutagen

from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DBFILE = 'music_info.db'
FORMATS = ['.flac', '.mp3', '.wma', '.ogg']
MUSIC_FOLDER = '/home/guilherme/Música'

engine = create_engine('sqlite:///{0}'.format(DBFILE), echo=True)
Base = declarative_base()

class Music(Base):
	__tablename__ = 'musics'

	id = Column(Integer, primary_key=True)
	title = Column(String)
	artist = Column(String)
	album = Column(String)
	path = Column(String)

	def __init__(self, title, artist, album, path):
		self.title = title
		self.artist = artist
		self.album = album
		self.path = path

Base.metadata.create_all(engine)

class MusicScanner(object):

	def scan_folder(self):
		for root, dirs, files in os.walk(MUSIC_FOLDER):
			for filee in files:
				if os.path.splitext(filee)[1].lower() in FORMATS:
					self.insert_in_db(root, filee)

	def insert_in_db(self, root, filee):
		path = os.path.join(root, filee).decode('utf-8')
		tag = mutagen.File(path, easy=True)

		title = tag['title'][0]  if tag.has_key('title') else 'Desconhecido'
		album = tag['album'][0]  if tag.has_key('album') else 'Desconhecido'
		artist = tag['artist'][0] if tag.has_key('artist') else 'Desconhecido'

		Session = sessionmaker(bind=engine)
		session = Session()
		music = Music(title, artist, album, path)

		session.add(music)
		session.commit()

x = MusicScanner()
x.scan_folder()